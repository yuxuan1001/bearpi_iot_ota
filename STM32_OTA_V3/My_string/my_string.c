
#include "my_string.h"


/**
*@brief	 	ip网络地址转换
*@param		adr：地址 ip：ip
*@return	无
*/
void smart_array(unsigned char* addr,unsigned char *ip)
{
    int i;
    char taddr[30];
    char * nexttok;
    char num;
    strcpy(taddr,(char *)addr);
    nexttok = taddr;
    for(i = 0; i < 4 ; i++)
    {
        nexttok = strtok(nexttok,".");
//		if(nexttok[0] == '0' && nexttok[1] == 'x') num = atoi16(nexttok+2,0x10);
//		else num = atoi16(nexttok,10);
        ip[i] = num;
        nexttok = NULL;
    }
}
/***********************************************************
  函数名称：Find_string(char *pcBuf,char*left,char*right, char *pcRes)
  函数功能：寻找特定字符串
  入口参数：
           char *pcBuf 为传入的字符串
           char*left   为搜索字符的左边标识符  例如："["
           char*right  为搜索字符的右边标识符  例如："]"
		   char *pcRes 为输出转存的字符串数组
  返回值：用来校验是否成功，无所谓的。
  备注： left字符需要唯一，right字符从left后面开始唯一即可
 服务器下发命令举例：+MQTTPUBLISH: 0,0,0,0,/device/NB/zx99999999999999_back,6,[reastrobot]
 此函数会操作内存空间  强烈建议 提前清空buf   memset(Find_token,0x00,sizeof(Find_token));
***********************************************************/
int Find_string(char *pcBuf,char *left,char *right, char *pcRes)
{
    char *pcBegin = NULL;
    char *pcEnd = NULL;
    pcBegin = strstr(pcBuf, left);               //找到第一次出现的位置
    pcEnd = strstr(pcBegin+strlen(left), right); //找到右边标识第一次出现位置
    if(pcBegin == NULL || pcEnd == NULL || pcBegin > pcEnd)
    {
        printf("string name not found!\n");
        return 0;
    }
    else
    {
        pcBegin += strlen(left);
        memcpy(pcRes, pcBegin, pcEnd-pcBegin);
        return 1;
    }
}

//int Smart_array(char *pcBuf,char *fu)
//{
//memcpy(&citc_server_ip[0],back_ip,strstr(back_ip,".")-back_ip);
//  char *pcBegin = NULL;
//	int8_t len =0;
//	pcBegin = strstr(pcBuf, fu);//找到第一次出现的位置
//	memcpy(&pcRes[i], pcBuf,pcBegin-pcBuf);
//	}
//	 len= strlen(pcBuf);
//	 pcBegin = strstr(pcBuf, fu);//找到第一次出现的位置
//	 memcpy(&pcRes[0], pcBuf,pcBegin-pcBuf);
//}

//寻找字符后面最近的结束符
//int find_end(char * usart_buffer, int number) {
//  int i;
//  for (i = 0; i < 100; i++) {
//    if ((usart_buffer[number + i] == '/') && (usart_buffer[number + i + 1] == '>')) {
//      return number + i+2 ;   //如果有换行符  需要加4
//    }
//  }
//}

//智能查找和匹配字符串
int8_t Find_AttributeStringAll(char *pcBuf, char *pcRest,uint16_t row,uint8_t col, char *left, char *right,uint8_t *Num)
{
    char pcFind[20] = {0};
    char *pcBegin = NULL;
    char *pcEnd = NULL;

    pcEnd  = pcBuf;
    while(1)
    {
        memset(pcFind,0,20);
        pcBegin = strstr(pcEnd+1, left);
        if((pcBegin == NULL) || (strcmp(pcBegin,"}") == 0))
        {
            printf("找到指定字符 !\n");
            return 1;
        }
        else
        {
            pcEnd = strstr(pcBegin+strlen(left), right);
            if ((pcEnd == NULL) || (pcBegin > pcEnd))
            {
                printf("Mail name not found!\n");
                return -1;
            }
            else
            {
                if(*Num>row)
                {
                    return 1;
                }
                else
                {
                    pcBegin += strlen(left);
                    memcpy(pcFind, pcBegin, pcEnd - pcBegin);
                    memset(pcRest+*Num*col,0,col);
                    memcpy(pcRest+*Num*col,pcFind,sizeof(pcFind));
                    (*Num) ++;
                }
            }
        }
    }
}

void Hex2Str(char *pbDest, char *pbSrc, int nLen)
{
    char    ddl,ddh;
    int i;

    for (i=0; i<nLen; i++)
    {
        ddh = 48 + pbSrc[i] / 16;
        ddl = 48 + pbSrc[i] % 16;
        if (ddh > 57) ddh = ddh + 7;
        if (ddl > 57) ddl = ddl + 7;
        pbDest[i*2] = ddh;
        pbDest[i*2+1] = ddl;
    }

    pbDest[nLen*2] = '\0';
}

void Str2Hex(char* str, char* hex)
{

    const char* cHex = "0123456789ABCDEF";
    int i=0;
    for(int j =0; j < strlen(str); j++)
    {
        unsigned int a =  (unsigned int) str[j];
        hex[i++] = cHex[(a & 0xf0) >> 4];
        hex[i++] = cHex[(a & 0x0f)];
    }
    hex[i] = '\0';
}

