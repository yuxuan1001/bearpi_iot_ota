#ifndef __WENZHENG_OTA_H_
#define __WENZHENG_OTA_H_

#include "main.h"
#include "usart.h"
#include "stdio.h"
#include "string.h"
#include <ctype.h>
#include <stdlib.h>

#define PageSize		FLASH_PAGE_SIZE			//2K

/*=====用户配置(根据自己的分区进行配置)=====*/
#define BootLoader_Size 		0xF000U			  ///< BootLoader的大小 60K 30 页
#define Application_Size		0x16800U	    ///< 应用程序的大小 90K    45页

#define Application_1_Addr		0x0800F000U		///< 应用程序1的首地址  30页
#define Application_2_Addr		0x08025800U		///< 应用程序2的首地址  75页
/*==========================================*/


/* 启动的步骤 */
#define Startup_Normol 0xFF	      ///< 正常启动
#define Startup_Update 0xAA	      ///< 升级再启动
#define Startup_Reset  0xBB       ///< ***恢复出厂 目前没使用***


int Erase_page(uint32_t pageaddr, uint32_t num);
void Set_Update_Down(void);
unsigned int Read_Start_Mode(void);
void Start_BootLoader(void);

void ReadFlash(uint32_t dest_addr, uint8_t * buff, int buf_len);
void WriteFlash(uint32_t addr, uint8_t * buff, int buf_len);

#endif



